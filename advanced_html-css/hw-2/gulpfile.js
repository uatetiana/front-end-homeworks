const gulp = require('gulp');
const styles = require('./gulp/tasks/build/styles');
const del = require('./gulp/tasks/build/del');
const serve = require('./gulp/tasks/dev/serve');
const img = require('./gulp/tasks/build/img');
const script = require('./gulp/tasks/build/script');
const icons = require('./gulp/tasks/build/icons');

const dev = gulp.parallel(styles);

module.exports.dev = gulp.series(del, dev, img, script, icons, serve);

module.exports.build = gulp.series(del, dev, img, script, icons);

