const gulp = require('gulp');
const server = require("browser-sync").create();

function browserSync(cb) {
    server.reload();
    cb();
}

module.exports = function serve(cb) {
    server.init({
            server: {
                baseDir: "./"
        },
        notify: false,
        open: true,
        cors: true,
    })
    gulp.watch('index.html', gulp.series(browserSync));
    return cb();
}