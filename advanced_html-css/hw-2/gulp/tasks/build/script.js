const gulp = require('gulp');
const uglify = require('gulp-uglify');
const pipeline = require('readable-stream').pipeline;
const rename = require("gulp-rename");

module.exports = function script (cb) {
    return pipeline(
        gulp.src('src/js/*.js'),
        uglify(),
        rename({
                suffix: ".min",
            }),

        gulp.dest('dist/js')
    );
    cb();
};