const gulp = require("gulp");

module.exports = function icons(cb) { (
    gulp.src('src/icons/*')
        .pipe(gulp.dest('dist/icons'))
); cb()
}