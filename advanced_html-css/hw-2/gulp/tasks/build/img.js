const gulp = require("gulp");
const imagemin = require("gulp-imagemin");

// Optimize Images

module.exports = function images(cb) {(
    gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
); cb();
};

