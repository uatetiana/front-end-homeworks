import React, { useState, useEffect } from 'react';
import List from './components/List/List';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import './App.scss'
import Modal from "./components/Modal/Modal";
import {Switch, Route, Redirect} from "react-router-dom";
import FavoritePage from "./components/pages/FavoritePage/FavoritePage";
import PageNotFound from "./components/pages/PageNotFound/PageNotFound";


function App () {
  const [articles, setArticles] = useState([]);
  const [cart, setCart] = useState([]);
  const [favorite, setFavorite] = useState([]);
  const [isShown, setIsShown] = useState(false);
  const [articleToAdd, setArticleToAdd] = useState({});
  const [favoriteToDelete, setFavoriteToDelete] = useState({});
  const [cartToDelete, setCartToDelete] = useState({});
  const [text, setText] = useState('');
  const [header, setHeader] = useState('Add to Cart?');
  let modal;

  useEffect(() => {
    fetchItems();

    if (JSON.parse(localStorage.getItem('favorite'))) {
      setFavorite(JSON.parse(localStorage.getItem('favorite')))
    }

    if (JSON.parse(localStorage.getItem('cart'))) {
      setCart(JSON.parse(localStorage.getItem('cart')))
    }

    localStorage.setItem('cart', JSON.stringify(cart));
    localStorage.setItem('favorite', JSON.stringify(favorite));
  }, []);


  const requestUrl = '/albums.json';
  const fetchItems = async () => {
    const data = await fetch(requestUrl);

    const articles = await data.json();
    setArticles(articles);
  };
  const addToFavorite = (value) => {
        if (!favorite.includes(value)) {
            setFavorite(prev => (
                 [...prev, value]))
        } else {
            let index = favorite.map(e => e.article).indexOf(value.article)
           favorite.splice(index, 1);
           setFavorite(prev => (
                [...prev]))
        }
    }
    const addToCart = () => {
        if (!cart.includes(articleToAdd)) {
           setCart(prev => (
                 [...prev, articleToAdd]
            ))
            setHeader('Do you want to add to cart?')
            updateStatus()
        } else {
            updateStatus();
        }
    }
    const updateStatus = (value, value2) => {
        if (value === 'Add to cart') {
            setIsShown(!isShown)
            setHeader('Add to Cart?')
            setArticleToAdd(value2)
        } else if (!value) {
            setIsShown(!isShown)
        }
    }


    const deleteItem = (value, value2) => {
        if (value === 'favorite') {

            setIsShown(!isShown)
            setHeader('Delete item from favorite?')
            setFavoriteToDelete(value2)


        }
        if (value === 'cart') {

            setIsShown(!isShown)
            setHeader('Delete item from cart?')
            setCartToDelete(value2)

        }
    }
    const deleteFromFavorite = () => {
        addToFavorite(favoriteToDelete)
        updateStatus()
    }

    const deleteFromCart = () => {
        let index = cart.map(e => e.article).indexOf(cartToDelete.article)
       cart.splice(index, 1);
        setCart(prev => (
           [...prev]
        ))
        updateStatus();
    }



    if (isShown) {
    modal = <Modal cart={cart} addToCart={addToCart} deleteFromFavorite={deleteFromFavorite}
                   deleteFromCart={deleteFromCart} updateStatus={updateStatus}
                   actions={['Ok', 'Cancel']} closeButton={true}
                   header={header}
                   text={text}/>
  } else {
    modal = null;
  }

  return (
      <>
    <Header cart={cart}/>
    <Switch>
      <Redirect exact from='/' to='/home'/>
      <Route exact path="/home" render={() => <List addToFavorite={addToFavorite} favorite={favorite}
                                                    keyName={'homePage'} homePage={true}
                                                    addToCart={addToCart}
                                                    updateStatus={updateStatus}
                                                    articles={articles}/>}/>
      <Route path="/favorite"
             render={() => <FavoritePage deleteItem={deleteItem} keyName={'favorite'}
                                         addToFavorite={addToFavorite} favorite={favorite}/>}/>
      <Route path="/cart"
             render={() => <List deleteItem={deleteItem} addToFavorite={addToFavorite}
                                 deleteType={'cart'} favorite={favorite} homePage={false} keyName={'cart'}
                                 addToCart={addToCart} updateStatus={updateStatus}
                                 articles={cart}/>}/>
      <Route path="*" component={PageNotFound}/>
    </Switch>
    <Footer/>
    {modal}
  </>
  )
}

export default App;