import React from 'react';
import List from "../../List/List";

function FavoritePage ({keyName, addToFavorite, favorite, addToCart, deleteItem  }) {
        return (
            <div>
                <List keyName={keyName} deleteType={'favorite'} deleteItem={deleteItem} addToFavorite={addToFavorite} favorite={favorite} addToCart={addToCart} articles={favorite}/>
            </div>
        );
}

export default FavoritePage;