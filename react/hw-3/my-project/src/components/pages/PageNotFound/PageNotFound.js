import React, {Component} from 'react';

class PageNotFound extends Component {
    render() {
        return (
            <div>
                <h3>Page NOT FOUND</h3>
            </div>
        );
    }
}

export default PageNotFound;