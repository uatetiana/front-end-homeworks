import React from 'react';
import ListItem from '../ListItem/ListItem';
import PropTypes from 'prop-types';

function List ({ articles, addToCart, updateStatus, homePage, favorite, keyName, deleteItem, deleteType, addToFavorite } ) {

    const articlesList = articles.map(el =>
        <ListItem deleteItem={deleteItem}
                  deleteType={deleteType}
                  homePage={homePage}
                  addToCart={addToCart}
                  favorite={favorite}
                  addToFavorite={addToFavorite}
                  updateStatus={updateStatus}
                  key={ keyName === 'cart' ? el.article + 'cart' : el.article + keyName } album={el}/>)

        return (
            <ul className={'articles-container'}>
                {articlesList}
            </ul>
        );
}

List.propTypes = {
    articles: PropTypes.array.isRequired,
    addToCart: PropTypes.func
}

export default List;
