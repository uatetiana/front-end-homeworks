import React from 'react';
import './Footer.scss';

function Footer () {
        return (
            <footer className={'footer'}>
                <h3>&copy; 2020 Music Store</h3>
            </footer>
        );
}

export default Footer;