import React from 'react';
import './ListItem.scss';
import Button from "../Button/Button";
import Favorite from '../../Favorite/Favorite';
import AddToFavorite from "../../AddToFavorite/AddToFavorite";
import PropTypes from 'prop-types';

function ListItem ({ album, favorite, homePage, deleteType, updateStatus, deleteItem, addToFavorite }) {

        let favoriteArticle = <Favorite/>
        let notFavorite = <AddToFavorite/>
        let buttonAddToCart = <Button updateStatus={updateStatus} article={album} color={'#9f2222'} text='Add to cart'/>
        let buttonDeleteItem = <button onClick={() => deleteItem(deleteType, album)} className={'btn-delete-item'}/>

        return (
            <li className={'articles-item'}>
                {!homePage && buttonDeleteItem}
                <img className={'articles__img'} src={album.url} alt=""/>
                <span onClick={() => {addToFavorite(album)
                }}>{favorite.map(e => e.article).includes(album.article)  ?  favoriteArticle : notFavorite}  </span>
                <h4 className={'articles__title'}>{album.value} <span
                    className={'articles__artist'}>by {album.artist}</span></h4>

                <p className={'articles__release-year'}>Release Year: {album.releaseYear} </p>
                <span className={'articles__price'}>{album.price} &euro;</span>
                {homePage && buttonAddToCart}
            </li>
        );
}

ListItem.prototypes = {
    album: PropTypes.object.isRequired,
    favorite: PropTypes.array.isRequired
}

export default ListItem;