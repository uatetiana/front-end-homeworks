import React from 'react';
import './Header.scss';
import Cart from "../../Cart/Cart";
import PropTypes from 'prop-types';
import Navigation from "../Navigation/Navigation";

function Header ({ cart }) {
        return (
            <header className={'header'}>
                <h3>Music Store</h3>
                <span id={cart.length} className={'header__cart'}><Cart /></span>
                <Navigation/>
            </header>
        );
}

Header.propTypes = {
    cart: PropTypes.array.isRequired
}

export default Header;