import React from "react";
import './Button.scss'

function Button ({color, text, updateStatus, article}){
        return <>
            <button className={'btn-start'} onClick={() => {
                updateStatus(text, article)
            }}
                    style={{backgroundColor: color}}>{text}</button>
        </>
}

export default Button;