import React, {useState} from "react";
import "./Modal.scss";
import PropTypes from 'prop-types';

function Modal({header, text, actions, updateStatus, closeButton, addToCart, deleteFromCart, deleteFromFavorite}) {

    const [isShown, setIsShown] = useState(true);

    let buttonAddDelete;
    if (header === 'Delete item from favorite?') {
        buttonAddDelete = <button onClick={() => deleteFromFavorite()} className={'btn'}>{actions[0]}</button>
    } else if (header === 'Delete item from cart?') {
        buttonAddDelete = <button onClick={() => deleteFromCart()} className={'btn'}>{actions[0]}</button>
    } else {
        buttonAddDelete = <button onClick={() => addToCart()} className={'btn'}>{actions[0]}</button>
    }

    const btn = closeButton ? <button onClick={() => {
        updateStatus(setIsShown(!isShown))
    }} className={'btn-close-modal'}/> : null

    return <div>
        <div className={'modal'} onClick={() => {
            updateStatus(setIsShown(!isShown))
        }}>
        </div>
        <div className={'modal__wrapper'}>
            <header className={'modal__header'}>
                <h1 className={'modal__title'}>{header}</h1>
                <div className={'btn-wrapper'}>{btn}</div>
            </header>
            <main className={'modal__main'}>
                <p className={'modal__text'}>{text}</p>
            </main>
            {buttonAddDelete}

            <button onClick={() => {
                updateStatus(setIsShown(!isShown))
            }} className={'btn'}>{actions[1]}</button>
        </div>
    </div>

}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string,
    actions: PropTypes.array.isRequired,
    updateStatus: PropTypes.func.isRequired,
    closeButton: PropTypes.bool.isRequired,
    addToCart: PropTypes.func.isRequired
}

export default Modal;