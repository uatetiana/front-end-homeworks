import React from 'react';
import {NavLink} from "react-router-dom";
import './Navigation.scss';

function Navigation () {
        return (
            <nav>
            <ul className={'nav'}>
                <li><NavLink className={'nav__link'} activeClassName={'nav__link__active'} exact to='/home'>Home</NavLink></li>
                <li><NavLink className={'nav__link'} activeClassName={'nav__link__active'} exact to='/favorite'>Favorite</NavLink></li>
                <li><NavLink className={'nav__link'} activeClassName={'nav__link__active'} exact to='/cart'>Cart</NavLink></li>
            </ul>
            </nav>
        );
}

export default Navigation;