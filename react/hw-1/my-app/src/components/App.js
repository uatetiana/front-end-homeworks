import React from "react";
import Button from "./Button/Button";
import "./App.scss"
import Modal from "./Modal/Modal"

class App extends React.Component {
    state = {
        modal1: false,
        modal2: false
    }

    render() {
        let modal1;
        let modal2;

        if (this.state.modal1) {

            modal1 = <Modal updateStatus={this.updateStatus} actions={['Ok', 'Cancel']} closeButton={true} header='Do you want to delete this file?'
                            text='Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'/>
        } else if (this.state.modal2) {
            modal2 =
                <Modal updateStatus={this.updateStatus} actions={['submit', 'Cancel']} closeButton={true}
                       header='Header' text='This is modal 2 text'/>
        } else {
            modal1 = modal2 = null;
        }


        return (
            <>
                <Button updateStatus={this.updateStatus} color='lightgreen' text="Open first modal"/>
                <Button updateStatus={this.updateStatus} color='lightblue' text="Open second modal"/>
                {modal1}
                {modal2}
            </>
        );

    }

    updateStatus = ( value) => {
        if (value === 'Open first modal') {
            this.setState({ modal1: !this.state.modal1})
        }
        if (value === 'Open second modal') {
            this.setState({modal2: !this.state.modal2})
        } else if (!value) {
            this.setState({modal1: value, modal2: value})
        }
    }
}


export default App;