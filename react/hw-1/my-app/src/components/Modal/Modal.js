import React from "react";
import "../App.scss"

class Modal extends React.Component {
    state = {
        isShown: true
    }

    render() {
        const {header, text, actions, updateStatus, closeButton} = this.props;

        const btn = closeButton ? <button onClick={() => {
            updateStatus(!this.state.isShown)
        }} className={'btn-close-modal'}/> : null

        return <div>
            <div className={'modal'} onClick={() => {
                updateStatus(!this.state.isShown)}}>
            </div>
            <div className={'modal__wrapper'}>
                <header className={'modal__header'}>
                    <h1 className={'modal__title'}>{header}</h1>
                    <div className={'btn-wrapper'}>{btn}</div>
                </header>
                <main className={'modal__main'}>
                    <p className={'modal__text'}>{text}</p>
                </main>
                <button className={'btn'}>{actions[0]}</button>
                <button className={'btn'}>{actions[1]}</button>
            </div>
        </div>
    }
}

export default Modal;