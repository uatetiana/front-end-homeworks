import React, {Component} from 'react';
import './Footer.scss';

class Footer extends Component {
    render() {
        return (
            <footer className={'footer'}>
                <h3>&copy; 2020 Music Store</h3>
            </footer>
        );
    }
}

export default Footer;