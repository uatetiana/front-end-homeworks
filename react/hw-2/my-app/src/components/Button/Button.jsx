import React from "react";
import './Button.scss'

class Button extends React.Component {

    render() {
        const {color, text, updateStatus, article} = this.props;

        return <>
            <button className={'btn-start'} onClick={() => {
                updateStatus(this.props.text, article)
            }}
                    style={{backgroundColor: color}}>{text}</button>
        </>
    }
}

export default Button;