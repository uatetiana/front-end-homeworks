import React, {Component} from 'react';
import './Header.scss';
import Cart from "../../Cart/Cart";
import PropTypes from 'prop-types';

class Header extends Component {

    render() {
        const { cart } = this.props;

        return (
            <header className={'header'}>
                <h3>Music Store</h3>
                <span id={cart.length} className={'header__cart'}><Cart /></span>
            </header>
        );
    }
}

Header.propTypes = {
    cart: PropTypes.array.isRequired
}

export default Header;