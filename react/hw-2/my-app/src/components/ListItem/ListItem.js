import React, {Component} from 'react';
import './ListItem.scss';
import Button from "../Button/Button";
import Favorite from '../../Favorite/Favorite';
import AddToFavorite from "../../AddToFavorite/AddToFavorite";
import PropTypes from 'prop-types';

class ListItem extends Component {
    render() {
        const {album, favorite} = this.props;
        let favoriteArticle = <Favorite/>
        let notFavorite = <AddToFavorite/>

        return (
            <li className={'articles-item'}>
                <img className={'articles__img'} src={album.url} alt=""/>
                <span onClick={() => {
                    this.props.addToFavorite(album.article)
                }}>{favorite.includes(album.article) ? favoriteArticle : notFavorite}  </span>

                <h4 className={'articles__title'}>{album.value} <span
                    className={'articles__artist'}>by {album.artist}</span></h4>

                <p className={'articles__release-year'}>Release Year: {album.releaseYear} </p>
                <span className={'articles__price'}>{album.price} &euro;</span>
                <Button updateStatus={this.props.updateStatus} article={album.article} color={'#9f2222'}
                        text='Add to cart'/>
            </li>
        );
    }
}

ListItem.prototypes = {
    album: PropTypes.object.isRequired,
    favorite: PropTypes.array.isRequired
}

export default ListItem;