import React, {Component} from 'react';
import ListItem from '../ListItem/ListItem';
import PropTypes from 'prop-types';

class List extends Component {
    state = {
        favorite: []
    }

    componentDidMount() {
        if (JSON.parse(localStorage.getItem('favorite'))) {
            this.setState({
                favorite: JSON.parse(localStorage.getItem('favorite'))
            })
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        localStorage.setItem('favorite', JSON.stringify(this.state.favorite));
    }

    render() {
        const {articles, addToCart, updateStatus} = this.props;

        const articlesList = articles.map(el =>
            <ListItem addToCart={addToCart} favorite={this.state.favorite} addToFavorite={this.addToFavorite}
                      updateStatus={updateStatus}
                      key={el.article} album={el}/>)

        return (
            <ul className={'articles-container'}>
                {articlesList}
            </ul>
        );
    }

    addToFavorite = (value) => {
        if (!this.state.favorite.includes(value)) {
            this.setState(prev => ({
                favorite: [...prev.favorite, value],
            }))
        } else {
            let index = this.state.favorite.indexOf(value);
            this.setState(prev => ({
                favorite: [...prev.favorite]
            }))
            this.state.favorite.splice(index, 1)
        }
    }
}

List.propTypes = {
    articles: PropTypes.array.isRequired,
    addToCart: PropTypes.func.isRequired
}

export default List;
