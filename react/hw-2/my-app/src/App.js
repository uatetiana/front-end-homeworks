import React, {Component} from 'react';
import List from './components/List/List';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import './App.scss'
import Modal from "./components/Modal/Modal";

class App extends Component {
    state = {
        articles: [],

        isShown: false,
        cart: [],
        valueToAdd: null,
        text: '',
        header: 'Add to Cart?'
    }

    componentDidMount() {
        const requestUrl = '/albums.json';
        const fetchPromise = fetch(requestUrl);
        fetchPromise.then(response => {
            return response.json()
        }).then(data => {
            this.getData(data)
        })

        if (JSON.parse(localStorage.getItem('cart'))) {
            this.setState({
                cart: JSON.parse(localStorage.getItem('cart'))
            })
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        localStorage.setItem('cart', JSON.stringify(this.state.cart));
    }

    render() {
        const {articles, cart, text, header} = this.state;
        let modal;

        if (this.state.isShown) {
            modal = <Modal cart={cart} addToCart={this.addToCart} updateStatus={this.updateStatus}
                           actions={['Ok', 'Cancel']} closeButton={true}
                           header={header}
                           text={text}/>
        } else {
            modal = null;
        }

        return (
            <>
                <Header cart={cart}/>
                <List addToCart={this.addToCart}
                      updateStatus={this.updateStatus}
                      articles={articles}/>
                <Footer/>
                {modal}
            </>
        );
    }

    getData = (data) => {
        this.setState({
            articles: data
        })
    }

    updateStatus = (value, value2) => {
        if (value === 'Add to cart') {
            this.setState(
                {
                    isShown: !this.state.isShown,
                    valueToAdd: value2
                })
        } else if (!value) {
            this.setState({isShown: !this.state.isShown})
        }
    }
    addToCart = () => {
        if (!this.state.cart.includes(this.state.valueToAdd)) {
            this.setState(prev => ({
                cart: [...prev.cart, this.state.valueToAdd],
                header: 'Do you want to add to cart?'
            }))
            this.updateStatus()
        } else {
            this.updateStatus();
            this.setState({
                header: 'Already exists'
            })
        }
    }
}

export default App;