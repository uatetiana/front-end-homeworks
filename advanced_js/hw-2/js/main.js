
const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

let root = document.querySelector('#root');
let ul = document.createElement('ul');
root.append(ul);

function UserException(message) {
    this.message = message;
    this.name = "Исключение, определенное пользователем";
}

function renderList(books) {
    for (let i = 0; i < books.length; i++) {
        try {
            renderElement(books, i);
        } catch (error) {
            console.log(error);
        }
    }
}

function renderElement(books, index) {
    const {author, name, price} = books[index];
    const arr = [author, name, price];


    if (!arr.includes(undefined)) {
        const li = document.createElement('li');
        ul.append(li);
        li.innerText = `автор: ${author}, наименование: "${name}", цена: ${price}`;
    } else {
        throw new UserException(`undefined field at books[${index}] at obj key[${arr.indexOf(undefined)}]`);
    }
}

renderList(books);