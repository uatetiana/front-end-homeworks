
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }

    set name(value) {
        if (!value) { throw new Error('Invalid name');}
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        if (!value) { throw new Error('Invalid age');}
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        if (!value) { throw new Error('Invalid salary'); }
        this._salary = value;
    }
}

let empl = new Employee('Ivan', 20, 1000);
console.log(empl.name);
console.log(empl.age);
console.log(empl.salary);
console.log(empl);


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age);
        this._name = name;
        this._age = age;
        this._salary = salary;
        this._lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }
    set salary(value) {
        if (!value) { throw new Error('Invalid salary'); }
        this._salary = value;
    }
    get lang() {
        return this._lang;
    }
    set lang(value) {
        this._lang = value;
    }
}

let programmer1 = new Programmer('Sam', 25, 1500, 'Java');
console.log(programmer1);
console.log(programmer1.name);
console.log(programmer1.age);
console.log(programmer1.salary);
console.log(programmer1.lang);

let programmer2 = new Programmer('Petro', 27, 1200, 'C#');
console.log(programmer2);
console.log(programmer2.name);
console.log(programmer2.age);
console.log(programmer2.salary);
console.log(programmer2.lang);