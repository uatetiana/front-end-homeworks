
const requestUrl = 'http://swapi.dev/api/films';
const ul = document.querySelector('.list');

async function requestList(url) {
    let response = await fetch(url);
    if (response.ok) { // если HTTP-статус в диапазоне 200-299
        // получаем тело ответа
        let json = await response.json();
        let dataList = await json.results;
        console.log(dataList)
        dataList.forEach(el => {
            createList(el);
        })
    } else {
        alert("Ошибка HTTP: " + response.status);
    }
}

requestList(requestUrl);

function createList(el) {
    let li = document.createElement('li');
    let {title, episode_id, opening_crawl} = el;
    li.innerHTML = `Title: ${title},<br>
    Episode: ${episode_id},<br>
    Description: <i>${opening_crawl}</i>`
    ul.append(li);
    let ul1 = document.createElement('ul');
    ul1.innerHTML = '<b>Characters:</b>'
    li.append(ul1);
    el.characters.forEach(item => {
        requestCharacters(item, ul1)
    })
}

async function  requestCharacters(url, ul1) {
    let response = await fetch(url);
    if (response.ok) { // если HTTP-статус в диапазоне 200-299
        // получаем тело ответа
        let json = await response.json();
        let li2 = document.createElement('li');
        li2.innerHTML = json.name;
        ul1.append(li2)
    } else {
        alert("Ошибка HTTP: " + response.status);
    }
}

