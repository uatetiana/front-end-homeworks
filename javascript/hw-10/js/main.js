
let passwordForm = document.querySelector('.password-form');
let passwordInputs = passwordForm.querySelectorAll("input[type='password']");
let span;


passwordForm.addEventListener('click', (event) => {
    if (event.target.classList.contains('fa-eye-slash')) {
        event.target.previousElementSibling.type = 'text';
        event.target.className = 'fas fa-eye icon-password'

    } else if (event.target.classList.contains('fa-eye')) {

        event.target.className = 'fas fa-eye-slash icon-password';
        event.target.previousElementSibling.type = 'password';
    }

    if (event.target.classList.contains('btn')) {
        event.preventDefault();
        if (passwordInputs[0].value === passwordInputs[1].value) {
            alert('Welcome');
        } else {
            span = document.createElement('span')
            passwordForm.append(span);
            span.classList.add('error-msg');
            span.innerText = 'Нужно ввести одинаковые значения';
        }
    }
})


