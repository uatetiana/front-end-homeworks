let firstNumber, secondNumber, min, max;

while (firstNumber % 1 !== 0 ) {
    firstNumber = +prompt('Please enter 1st number', '');
}

while (secondNumber % 1 !== 0) {
    secondNumber = +prompt('Please enter 2nd number', '');
}


if (secondNumber < firstNumber) {
    min = secondNumber;
    max = firstNumber;
} else {
    min = firstNumber;
    max = secondNumber;
}


for (let i = min; i <= max; i++) {

    if (i > 1) {
        let isSimple = true;
        for (let j = 2; j < i; j++) {
            if (i % j === 0) {
                isSimple = false;
                break;
            }
        }
        if (isSimple) {
            console.log(i);
        }
    }

}