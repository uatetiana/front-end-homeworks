function filterBy(arr, dataTypeToOmit) {

    let maybeNull = null;

    let cleanArr = arr.filter(elem => {
            if (dataTypeToOmit !== maybeNull && !Array.isArray(dataTypeToOmit)) {
                if (typeof elem !== typeof dataTypeToOmit) {
                    return true;
                }
            } else if (dataTypeToOmit === maybeNull) {
                if (elem !== null) {
                    return true;
                }
            } else {
                if (!Array.isArray(elem)) {
                    return true;
                }
            }
        }
    )
    return cleanArr;
}


let testArr = ['hello', 'world', 23, '23', null, {name: 'object'}, [1, 2, 3]];


let testData = 'string';
let testData1 = 1;
let testData2 = null;
let testData3 = [0, 1, 2];

let result = filterBy(testArr, testData);
let result1 = filterBy(testArr, testData1);
let result2 = filterBy(testArr, testData2);
let result3 = filterBy(testArr, testData3);
console.log(result);
console.log(result1);
console.log(result2);
console.log(result3);