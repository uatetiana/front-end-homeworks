$(document).ready(function () {
    $('.header-link').on('click', function (event) {
        let offsetStart = $(this).offset().top;
        if (this.hash !== '') {
            event.preventDefault();
        }
        let hash = this.hash;
        let index = $('#news').offset().top;
        let id = hash.slice(1);
        let offsetEnd = $(`#${id}`).offset().top;
        let result = offsetEnd - offsetStart;

        if (result < 200) {
            result *= 10
        } else if (result < 500) {
            result *= 5;
        }

        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, (result * .53), function () {
            window.location.hash = hash;
        })
    })
})

let btnTop = $('.btn-top');
let endScroll;

$(this).scroll(function () {
    endScroll = $(this).scrollTop()
    if ($(this).scrollTop() > $(this).outerHeight()) {
        btnTop.addClass('active');
    } else {
        btnTop.removeClass('active');
    }
})

btnTop.on('click', function () {
    $('html, body').animate({
        scrollTop: $(document.body).position().top
    }, endScroll * .53)
})


$('.btn-toggle').on('click', function (event) {
    $('section#magazine').slideToggle('slow')
})

