let btnChangeTheme = document.querySelector('.btn-theme');
let bodyTag = document.querySelector('body');
let tableBg = document.querySelector('.table');
let tableBorderBg = document.querySelector('.background');
let tableFontsTitleSubt = document.querySelectorAll('.table__title, .table__subtitle, .table__item');
let tableFooterContact = document.querySelector('.table__btn-contact');
let tableFooterStndPrem = document.querySelectorAll('.table__btn-standard,.table__btn-premium');
let tableFooterUlt = document.querySelector('.table__btn-ultimate');
let tableRowsEven = document.querySelectorAll('.table__body_row:nth-child(even)');
let tableRowsOdd = document.querySelectorAll('.table__body_row:nth-child(odd)');

let isDarkMode = false;

function darkTheme() {
    bodyTag.classList.add('dark');
    tableBg.classList.add('dark');
    btnChangeTheme.classList.add('dark');
    tableRowsEven.forEach(el => {
        el.classList.add('dark');
    })
    tableRowsOdd.forEach(el => {
        el.classList.add('dark');
    })
    tableBorderBg.classList.add('dark');

    tableFontsTitleSubt.forEach(el => {
        el.classList.add('dark');
    })
    tableFooterContact.classList.add('dark');
    tableFooterStndPrem.forEach(el => {
        el.classList.add('dark');
    })
    tableFooterUlt.classList.add('dark');
}

function lightTheme() {
    bodyTag.classList.remove('dark');
    tableBg.classList.remove('dark');
    btnChangeTheme.classList.remove('dark');
    tableRowsEven.forEach(el => {
        el.classList.remove('dark');
    })
    tableRowsOdd.forEach(el => {
        el.classList.remove('dark');
    })
    tableBorderBg.classList.remove('dark');

    tableFontsTitleSubt.forEach(el => {
        el.classList.remove('dark');
    })
    tableFooterContact.classList.remove('dark');
    tableFooterStndPrem.forEach(el => {
        el.classList.remove('dark');
    })
    tableFooterUlt.classList.remove('dark');
}

btnChangeTheme.addEventListener('click', ()=> {

    if (!isDarkMode) {
        darkTheme();
        localStorage.setItem('isDarkMode', 'true')
        isDarkMode = true;
    } else {
       lightTheme();
        isDarkMode = false;
        localStorage.setItem('isDarkMode', 'false')
    }
})


if (localStorage.getItem('isDarkMode') === 'true') {
    darkTheme();
    localStorage.setItem('isDarkMode', 'true')
} else {
    lightTheme();
    isDarkMode = false;
    localStorage.setItem('isDarkMode', 'false');
}



