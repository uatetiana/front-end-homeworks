const body = document.querySelector('body');
const form = document.createElement('form');
const div = document.createElement('div');
const label = document.createElement('label');
const input = document.createElement('input');
const div1 = document.createElement('div');
const p = document.createElement('p');
const errorMessage = document.createElement('p');
const span = document.createElement('span');

let buttonCross;

form.classList.add('form');
div.classList.add('form-wrapper');
label.classList.add('form-price-label');
input.classList.add('form-price-inp');
p.classList.add('form-price-container');
errorMessage.classList.add('error-message-container')

label.setAttribute('for', 'price');
input.setAttribute('id', 'price');
input.setAttribute('type', 'text');

label.innerText = 'Price, USD ';


form.append(div);
div.append(div1);
div1.append(p, label, input, errorMessage);
body.prepend(form);

input.addEventListener('focus', (event) => {
    input.classList.add('focus');
})

input.addEventListener('blur', (event) => {
    let price = +event.target.value;

    if (price <= 0) {
        input.classList.add('error');
        errorMessage.innerHTML = 'Please enter correct price';
        errorMessage.classList.remove('hidden');
    } else {
        input.classList.remove('focus');
        span.classList.add('form-price-item');
        span.innerHTML = price;
        buttonCross = document.createElement('button');
        buttonCross.classList.add('form-price-item-remove');
        buttonCross.innerHTML = 'x';
        span.append(buttonCross);
        p.prepend(span);
        input.classList.remove('error');
        errorMessage.classList.add('hidden');
        buttonCross.addEventListener('click', event => {
            event.preventDefault();
            event.target.parentNode.remove();
        })
    }
})





