let mainContainer = document.querySelector('.images-wrapper');
let imgCollection = document.querySelectorAll('.image-to-show');
let stopBtn, restartBtn;
stopBtn = document.createElement('button');
restartBtn = document.createElement('button');
stopBtn.innerText = 'Прекратить';
restartBtn.innerText = 'Возобновить показ';
mainContainer.after(stopBtn, restartBtn);

let intervalId;
let isAnimationStopped;


stopBtn.addEventListener('click', () => {
    clearInterval(intervalId);
    isAnimationStopped = true;
    intervalId = 0;
})


restartBtn.addEventListener('click', (event) => {
    if (isAnimationStopped) {
        showImgs(currentImgIndex);
        isAnimationStopped = false;
    } else {
        event.preventDefault();
    }
})


let currentImgIndex = 1;
let previousImgIndex = currentImgIndex - 1;

function showImgs() {
    stopBtn.addEventListener('click', () => {
        clearInterval(intervalId);
    })

    intervalId = setInterval(() => {
        imgCollection[currentImgIndex].classList.add('active');
        if (currentImgIndex > 0 || previousImgIndex === 3) {
            imgCollection[previousImgIndex].classList.remove('active')
        }
        currentImgIndex++;
        previousImgIndex++;
        if (currentImgIndex === 4) {
            setTimeout(() => {
                imgCollection[imgCollection.length - 1].classList.remove('active')
            }, 999)
            currentImgIndex = 0;
            previousImgIndex = currentImgIndex - 1;
        }
    }, 1000)
    return intervalId;
}

showImgs();

