function createList(arr) {

    const ul = document.createElement('ul');

    const body = document.querySelector('body');

    let newArr = arr.map(function (elem) {
        let li = document.createElement('li');
        li.innerHTML = elem;
        return li;
    });

    newArr.forEach(elem => {
        ul.append(elem);
    })
    body.prepend(ul);
}


let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
let arr1 = ['1', '2', '3', 'sea', 'user', 23];

createList(arr);
createList(arr1);