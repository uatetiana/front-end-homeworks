function createNewUser() {

    let firstName = prompt('Enter your first name', 'Ivan');
    let lastName = prompt('Enter you last name', 'Kravchenko');
    let birthday = prompt(`Enter you date of birth in format 'dd.mm.yyyy'`, '10.07.1990');

    let userTemplate = {
        firstName,
        lastName,
        getLogin: function () {
            console.log(this.firstName[0].toLowerCase() + this.lastName.toLowerCase());
        },
        birthday,
        getAge: function () {
            let correctFormatDate = [];
            let arr = this.birthday.split('.');
            correctFormatDate.push(arr[1], arr[0], arr[2]);
            let strBirthday = correctFormatDate.join('.');

            const now = new Date();
            const userBirthday = new Date(strBirthday);
            let userAge;

            if (now.getMonth() < userBirthday.getMonth()) {
                userAge = now.getFullYear() - userBirthday.getFullYear() - 1;
            } else if (now.getMonth() === userBirthday.getMonth()) {
                if (now.getDate() < userBirthday.getDate()) {

                    userAge = now.getFullYear() - userBirthday.getFullYear() - 1;
                } else {
                    userAge = now.getFullYear() - userBirthday.getFullYear();
                }

            } else {
                userAge = now.getFullYear() - userBirthday.getFullYear();
            }
            console.log(userAge);
        },
        getPassword: function () {
            console.log(this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substr(this.birthday.length - 4));
        }
    };
    return userTemplate;
}

let user1 = createNewUser();
user1.getAge();
user1.getPassword();
